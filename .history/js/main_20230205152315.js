import { dataGlasses, glassesSelect } from './controller.js';

let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<button class="col-4" onclick="glassesSelect('${item.virtualImg}')"><img src=${item.src} style="width:100%" /></button>`;
    contentHTML += contentItem;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};
renderGlassesList();
