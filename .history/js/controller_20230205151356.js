import { dataGlasses } from './data.js';

let searchInfo = (id, glassesArr) => {
  for (let i = 0; i < glassesArr.length; i++) {
    let viTri = glassesArr[i];
    if (viTri.id == id) {
      return i;
    }
  }
  return -1;
};

export let glassesSelect = (idGlasses) => {
  let viTri = searchInfo(idGlasses, dataGlasses);
  if (viTri != -1) {
    let glasses = dataGlasses[viTri];
    let contentHTML = `
    <h4>${glasses.name} - <span>${glasses.brand} (${glasses.color})</span></h4>
    <button class="btn btn-danger">${glasses.price}</button> <span class="text-success">Stocking</span>
    <p>${glasses.description}</p>`;
    document.getElementById('glassesInfo').innerHTML = contentHTML;
    document.getElementById('glassesInfo').style.display = 'block';
    let content = `<img src="${glasses.virtualImg}" alt="avatar"/>`;
    document.getElementById('avatar').innerHTML = content;
  }
};

export let removeGlasses = (check) => {
  if (check) {
    document.getElementById('virGlass').style.display = 'block';
  } else {
    document.getElementById('virGlass').style.display = 'none';
  }
};
