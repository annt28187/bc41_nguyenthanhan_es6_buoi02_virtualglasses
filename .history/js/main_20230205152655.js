import { dataGlasses, glassSelect } from './controller.js';

let renderGlasses = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<button onclick="glassSelect('${item.id}')" class="col-4 glasses"><img src="${item.src}" style="width: 100%"/></button>`;
    contentHTML += contentItem;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};
renderGlasses();
