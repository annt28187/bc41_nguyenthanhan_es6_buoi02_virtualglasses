import { dataGlasses, glassesSelect } from './controller.js';

let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<div class="col-4" onclick="glassesSelect('${item.virtualImg}')"><img src=${item.src} style="width:100%" /></div>`;
    contentHTML += contentItem;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};
renderGlassesList();
