import { dataGlasses } from './data.js';
import { renderGlassesList, searchInfo, showGlassesFace, showInfoGlasses } from './controller.js';

renderGlassesList();

export let showImage = (id) => {
  let viTri = searchInfo(id, dataGlasses);
  showGlassesFace(dataGlasses[viTri]);
  showInfoGlasses(dataGlasses[viTri]);
};

function BeforeRemoveGlasses() {
  document.getElementById('glassesInfo').style.display = 'block';
  document.getElementById('glassesInfo').style.display = 'block';
}
function AfterRemoveGlasses() {
  document.getElementById('glassesInfo').style.display = 'none';
  document.getElementById('glassesInfo').style.display = 'none';
}

window.BeforeRemoveGlasses = BeforeRemoveGlasses;
window.AfterRemoveGlasses = AfterRemoveGlasses;
window.showImage = showImage;
