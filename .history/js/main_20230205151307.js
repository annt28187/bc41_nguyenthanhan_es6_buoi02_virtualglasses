import { renderGlassesList } from './controller.js';
import { dataGlasses } from './data.js';

renderGlassesList(dataGlasses);
