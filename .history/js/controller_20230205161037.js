export const renderListGlass = (arr) => {
  let contentHTML = '';
  for (const item of arr) {
    let contentImg = `<img style="width: 100%" src="${item.src}" id="${item.id}" class="col-4 glasses_check"  alt=""/> `;

    contentHTML += contentImg;
  }

  document.getElementById('vglassesList').innerHTML = contentHTML;
};

export const handleClickGlasses = (arr) => {
  let ch = document.querySelectorAll('.glasses_check');

  for (const click of clickList) {
    click.addEventListener('click', () => {
      let contentHTML = '';
      let contentInfoHTML = '';
      for (const glasses of arr) {
        if (glasses.id === click.id) {
          let contentImg = `<img src="${glasses.virtualImg}" alt="" />`;
          let contentInfo = `<h4>${glasses.brand} - ${glasses.name} (${glasses.color})</h4>
                <button class="btn btn-danger">${glasses.price}</button>
                <span class="text-success">Stocking</span>
                <p>${glasses.description}</p>`;

          contentHTML += contentImg;
          contentInfoHTML += contentInfo;
          document.getElementById('avatar').innerHTML = contentHTML;
          document.getElementById('glassesInfo').innerHTML = contentInfoHTML;
          document.querySelector('.vglasses__info').style.display = 'block';
        }
      }
    });
  }
};
