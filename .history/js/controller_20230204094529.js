import { dataGlasses } from './data.js';

export let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<button onclick="glassSelect('${item.id}')" class="col-4 glasses"><img src="${item.src}" style="width: 100%"/></button>`;
    contentHTML += contentItem;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};

let searchInfo = (src, glassesArr) => {
  return glassesArr.findIndex((item) => {
    return item.virtualImg == src;
  });
};

let glassesItem = (src) => {
  document.querySelector('#avatar').innerHTML = `<img src=${src} alt="" />`;
  let index = searchInfo(src, dataGlasses);
  let glassesInfo = dataGlasses[index];
  let contentInfo = `
    <h2>${glassesInfo.name} - <span>${glassesInfo.brand} (${glassesInfo.color})</span></h2>
    <div class="price">$${glassesInfo.price} </div><span>Available</span>
    <div class="description mt-2">${glassesInfo.description}</div>
  `;
  if (viTri != -1) {
    let contentHTML = `<h5>${dataGlasses[viTri].name} - ${dataGlasses[viTri].brand} (${dataGlasses[viTri].color})</h5>
                           <span class="glass-price">$${dataGlasses[viTri].price}</span>
                           <p>${dataGlasses[viTri].description}</p>
                        `;
    document.getElementById('glassesInfo').innerHTML = contentHTML;
    document.getElementById('glassesInfo').style.display = 'block';
    let glassEl = `<img id="virGlass" src="${dataGlasses[viTri].virtualImg}" alt="" />`;
    document.getElementById('avatar').innerHTML = glassEl;
  }
};

let removeGlasses = (check) => {
  if (check) {
    document.getElementById('virGlass').style.display = 'block';
  } else {
    document.getElementById('virGlass').style.display = 'none';
  }
};
