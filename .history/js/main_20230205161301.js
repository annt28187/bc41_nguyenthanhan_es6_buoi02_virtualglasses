import { dataGlasses } from './data.js';
import { renderListGlass, handleSelectGlasses } from './controller.js';

renderListGlass(dataGlasses);
handleSelectGlasses(dataGlasses);

document.getElementById('before').addEventListener('click', (event) => {
  document.getElementById('imageFace').style.display = 'none';
  document.querySelector('.vglasses__info').style.display = 'none';
});

document.getElementById('after').addEventListener('click', (event) => {
  document.getElementById('imageFace').style.display = 'block';
  document.querySelector('.vglasses__info').style.display = 'block';
});
