import { dataGlasses } from './data.js';
import { renderGlassesList, searchInfo, showGlassesFace, showInfoGlasses } from './controller.js';

renderGlassesList(dataGlasses);

export let showImage = (id) => {
  let viTri = searchInfo(id, dataGlasses);
  showGlassesFace(dataGlasses[viTri]);
  showInfoGlasses(dataGlasses[viTri]);
};

let removeGlasses = (check) => {
  if (check == true) {
    document.getElementById('facekinh').style.display = 'block';
    document.getElementById('glassesInfo').style.display = 'block';
  } else {
    document.getElementById('facekinh').style.display = 'none';
    document.getElementById('glassesInfo').style.display = 'none';
  }
};

function BeforeRemoveGlasses() {
  document.getElementById('facekinh').style.display = 'block';
  document.getElementById('glassesInfo').style.display = 'block';
}
function AfterRemoveGlasses() {
  document.getElementById('facekinh').style.display = 'none';
  document.getElementById('glassesInfo').style.display = 'none';
}

window.BeforeRemoveGlasses = BeforeRemoveGlasses;
window.AfterRemoveGlasses = AfterRemoveGlasses;
window.showImage = showImage;
