import { dataGlasses } from './data.js';
import { renderGlassesList, removeGlasses } from './controller.js';
renderGlassesList();
removeGlasses(dataGlasses);
