import { dataGlasses } from './data.js';

export let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<button onclick="glassSelect('${item.id}')" class="col-4 glasses"><img src="${item.src}" style="width: 100%"/></button>`;
    contentHTML += contentItem;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};

let timKiemViTri = (id, arr) => {
  var viTri = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].id == id) {
      viTri = i;
    }
  }
  return viTri;
};

let glassSelect = (idGlass) => {
  var viTri = timKiemViTri(idGlass, dataGlasses);
  if (viTri != -1) {
    let contentHTML = `<h5>${dataGlasses[viTri].name} - ${dataGlasses[viTri].brand} (${dataGlasses[viTri].color})</h5>
                           <span class="glass-price">$${dataGlasses[viTri].price}</span>
                           <p>${dataGlasses[viTri].description}</p>
                        `;
    document.getElementById('glassesInfo').innerHTML = contentHTML;
    document.getElementById('glassesInfo').style.display = 'block';
    let glassEl = `<img id="virGlass" src="${dataGlasses[viTri].virtualImg}" alt="" />`;
    document.getElementById('avatar').innerHTML = glassEl;
  }
};

let removeGlasses = (check) => {
  if (check) {
    document.getElementById('virGlass').style.display = 'block';
  } else {
    document.getElementById('virGlass').style.display = 'none';
  }
};
