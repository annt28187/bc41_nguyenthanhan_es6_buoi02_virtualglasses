import { dataGlasses } from './data.js';

export let renderGlassesList = (dataGlasses) => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<div class="col-4" onclick="glassesItem('${item.virtualImg}')"><img src=${item.src} style="width:100%" /></div>`;
    contentHTML += contentItem;
  });
  document.querySelector('#vglassesList').innerHTML = contentHTML;
};

export let searchInfo = (src, glassesArr) => {
  return glassesArr.findIndex((item) => {
    return item.virtualImg == src;
  });
};

export let glassesItem = (src) => {
  document.querySelector('#avatar').innerHTML = `<img src=${src} alt="" />`;
  let index = searchInfo(src, dataGlasses);
  let glassesInfo = dataGlasses[index];
  let contentHTML = `
    <h4>${glassesInfo.name} - <span>${glassesInfo.brand} (${glassesInfo.color})</span></h4>
    <button class="btn btn-danger">${glasses.price}</button> <span class="text-success">Stocking</span>
    <p>${glassesInfo.description}</p>
  `;
  document.querySelector('#glassesInfo').innerHTML = contentHTML;
  document.querySelector('.vglasses__info').style.display = 'block';
};

export const removeGlasses = (item) => {
  item
    ? (document.querySelector('#avatar img').style.display = 'block')
    : (document.querySelector('#avatar img').style.display = 'none');
};
