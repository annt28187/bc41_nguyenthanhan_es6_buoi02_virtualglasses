import { dataGlasses } from './data.js';
import { showImage } from './main.js';

export let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<div class="col-4" onclick="showImage('${item.virtualImg}')"><img src=${item.src} style="width:100%" /></div>`;
    contentHTML += contentItem;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};

let searchInfo = (id, glassesArr) => {
  for (let i = 0; i < glassesArr.length; i++) {
    let viTri = glassesArr[i];
    if (viTri.id == id) {
      return i;
    }
  }
  return -1;
};

export let glassesSelect = (idGlasses) => {
  let viTri = searchInfo(idGlasses, dataGlasses);
  if (viTri != -1) {
    let glasses = dataGlasses[viTri];
    let contentHTML = `
    <h4>${glasses.name} - <span>${glasses.brand} (${glasses.color})</span></h4>
    <button class="btn btn-danger">${glasses.price}</button> <span class="text-success">Stocking</span>
    <p>${glasses.description}</p>`;
  }
};

export let showGlassesFace = (glasses) => {
  let content = `
    <div ">
        <img src="${glasses.virtualImg}" alt="avatar" id="facekinh"/>
    </div>`;
  document.getElementById('avatar').innerHTML = content;
};

export let showInfoGlasses = (glasses) => {
  let contentHTML = `
    <h4>${glasses.name} - <span>${glasses.brand} (${glasses.color})</span></h4>
    <button class="btn btn-danger">${glasses.price}</button> <span class="text-success">Stocking</span>
    <p>${glasses.description}</p>
  `;
  document.getElementById('glassesInfo').innerHTML = content;
  document.getElementById('glassesInfo').style.display = 'block';
};

let removeGlasses = (check) => {
  if (check == true) {
    document.getElementById('facekinh').style.display = 'block';
    document.getElementById('glassesInfo').style.display = 'block';
  } else {
    document.getElementById('facekinh').style.display = 'none';
    document.getElementById('glassesInfo').style.display = 'none';
  }
};
