import { dataGlasses } from './data.js';
import { renderListGlass, handleClickGlasses } from './controller.js';

renderListGlass(dataGlasses);
handleClickGlasses(dataGlasses);

document.getElementById('before').addEventListener('click', (event) => {
  document.getElementById('glassesInfo').style.display = 'none';
  document.querySelector('.vglasses__info').style.display = 'none';
});

document.getElementById('after').addEventListener('click', (event) => {
  document.getElementById('glassesInfo').style.display = 'block';
  document.querySelector('.vglasses__info').style.display = 'block';
});
