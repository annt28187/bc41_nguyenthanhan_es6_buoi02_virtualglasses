export const renderListGlass = (arr) => {
  let contentHTML = '';
  for (const item of arr) {
    let contentImg = `<img style="width: 100%" src="${item.src}" id="${item.id}" class="col-4 glasses_select"  alt=""/> `;

    contentHTML += contentImg;
  }

  document.getElementById('vglassesList').innerHTML = contentHTML;
};

export const handleSelectGlasses = (arr) => {
  let selectList = document.querySelectorAll('.glasses_select');

  for (const select of selectList) {
    select.addEventListener('click', () => {
      let contentHTML = '';
      let contentInfoHTML = '';
      for (const glasses of arr) {
        if (glasses.id === select.id) {
          let contentImg = `<img id="imageFace" src="${glasses.virtualImg}" alt="" />`;
          let contentInfo = `<h4>${glasses.brand} - ${glasses.name} (${glasses.color})</h4>
                <button class="btn btn-danger">${glasses.price}</button>
                <span class="text-success">Stocking</span>
                <p>${glasses.description}</p>`;

          contentHTML += contentImg;
          contentInfoHTML += contentInfo;
          document.getElementById('avatar').innerHTML = contentHTML;
          document.getElementById('glassesInfo').innerHTML = contentInfoHTML;
          document.querySelector('.vglasses__info').style.display = 'block';
        }
      }
    });
  }
};
