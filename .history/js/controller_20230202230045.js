import { dataGlasses } from './data.js';

export let renderGlassList = () => {
  var contentHTML = '';
  dataGlasses.forEach((item) => {
    var content = `<button id='${item.id}' class='col-4' onclick="dressUp('${item.id}')"><img src="${item.src}"></button>`;
    contentHTML = contentHTML + content;
  });
  document.getElementById('vglassesList').innerHTML = contentHTML;
};
