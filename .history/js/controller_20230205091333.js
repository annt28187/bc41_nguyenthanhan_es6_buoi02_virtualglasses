import { dataGlasses } from './data.js';
import { showImage } from './main.js';

export let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<div class="col-4" onclick="glassesItem('${item.virtualImg}')"><img src=${item.src} style="width:100%" /></div>`;
    contentHTML += contentItem;
  });
  document.querySelector('#vglassesList').innerHTML = contentHTML;
};

export let searchInfo = (id, glassesArr) => {
  for (let i = 0; i < glassesArr.length; i++) {
    let viTri = glassesArr[i];
    if (viTri.id == id) {
      return i;
    }
  }
  return -1;
};

export let showGlassesFace = (glasses) => {
  let content = `
    <div ">
        <img src="${glasses.virtualImg}" alt="avatar" id="facekinh"/>
    </div>`;
  document.getElementById('avatar').innerHTML = content;
};

export let showInfoGlasses = (glasses) => {
  let contentHTML = `
    <h4>${glassesInfo.name} - <span>${glassesInfo.brand} (${glassesInfo.color})</span></h4>
    <button class="btn btn-danger">${glasses.price}</button> <span class="text-success">Stocking</span>
    <p>${glassesInfo.description}</p>
  `;
  document.getElementById('glassesInfo').innerHTML = content;
  document.getElementById('glassesInfo').style.display = 'block';
};

export const removeGlasses = (item) => {
  item
    ? (document.querySelector('#avatar img').style.display = 'block')
    : (document.querySelector('#avatar img').style.display = 'none');
};
