import { dataGlasses } from './data.js';

export let renderGlassesList = () => {
  let contentHTML = '';
  dataGlasses.forEach(function (item) {
    var contentItem = `<div class="col-4" onclick="glassesItem('${item.virtualImg}')"><img src=${item.src}  /></div>`;
    contentHTML += contentItem;
  });
  document.querySelector('#vglassesList').innerHTML = contentHTML;
};

let searchInfo = (src, glassesArr) => {
  return glassesArr.findIndex((item) => {
    return item.virtualImg == src;
  });
};

let glassesItem = (src) => {
  document.querySelector('#avatar').innerHTML = `<img src=${src} alt="" />`;
  let index = searchInfo(src, dataGlasses);
  let glassesInfo = dataGlasses[index];
  let contentHTML = `
    <h2>${glassesInfo.name} - <span>${glassesInfo.brand} (${glassesInfo.color})</span></h2>
    <div class="price">$${glassesInfo.price} </div><span>Available</span>
    <div class="description mt-2">${glassesInfo.description}</div>
  `;
  document.querySelector('#glassesInfo').innerHTML = contentHTML;
  document.querySelector('.vglasses__info').style.display = 'block';
};

const removeGlasses = (item) => {
  item
    ? (document.querySelector('#avatar img').style.display = 'block')
    : (document.querySelector('#avatar img').style.display = 'none');
};
