import { dataGlasses } from './data.js';
import { renderGlassesList, searchInfo, showGlassesFace, showInfoGlasses } from './controller.js';
renderGlassesList();

let showImage = (id) => {
  let viTri = searchInfo(id, dataGlasses);
  showGlassesFace(dataGlasses[viTri]);
  showInfoGlasses(dataGlasses[viTri]);
};
